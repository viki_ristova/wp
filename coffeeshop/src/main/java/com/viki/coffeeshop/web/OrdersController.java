package com.viki.coffeeshop.web;

        import com.viki.coffeeshop.model.Customer;
        import com.viki.coffeeshop.model.CustomerOrder;
        import com.viki.coffeeshop.model.Product;
        import com.viki.coffeeshop.persistence.CustomerRepository;
        import com.viki.coffeeshop.persistence.OrderRepository;
        import com.viki.coffeeshop.persistence.ProductRepository;
        import com.viki.coffeeshop.service.ProductService;
        import org.slf4j.Logger;
        import org.slf4j.LoggerFactory;
        import org.springframework.beans.factory.annotation.Autowired;
        import org.springframework.stereotype.Controller;
        import org.springframework.ui.Model;
        import org.springframework.web.bind.annotation.*;

        import java.util.HashSet;
        import java.util.Set;
/**
 * Created by Viki Ristova on 10/06/18.
 */


@Controller
public class OrdersController {

    @Autowired
    private ProductRepository productRepository;

    @Autowired
    private OrderRepository orderRepository;

    @Autowired
    private CustomerRepository customerRepository;

    @RequestMapping(value = "/orders", method = RequestMethod.GET)
    public String productsList(Model model){
        model.addAttribute("products", productRepository.findAll());
        model.addAttribute("orders", orderRepository.findAll());
        return "orders";
    }

    @RequestMapping(value="/createorder", method = RequestMethod.POST)
    @ResponseBody
    public String saveOrder(@RequestParam String firstName, @RequestParam String lastName, @RequestParam(value="productIds[]") Long[] productIds){

        Customer customer = new Customer();
        customer.setFirstName(firstName);
        customer.setLastName(lastName);
        customerRepository.save(customer);
        CustomerOrder customerOrder = new CustomerOrder();
        customerOrder.setCustomer(customerRepository.findById(customer.getCustomerId()).orElseThrow(() -> new IllegalArgumentException("Customer with that id does not exist!!")));
        Set<Product> productSet = new HashSet<>();
        for (Long productId:productIds){
            productSet.add(productRepository.findById(productId).orElseThrow(() -> new IllegalArgumentException("Product with that id does not exist!! first for loop")));
        }
        customerOrder.setProducts(productSet);
        Double total = 0.0;
        for (Long productId:productIds) {
            Product p=productRepository.findById(productId).orElseThrow(() -> new IllegalArgumentException("Product with that id does not exist!! second for loop"));
            total = total + p.getProductPrice();
        }
        customerOrder.setTotal(total);
        orderRepository.save(customerOrder);

        return customerOrder.getOrderId().toString();
    }

    @RequestMapping(value = "/removeorder", method = RequestMethod.POST)
    @ResponseBody
    public String removeOrder(@RequestParam Long Id){
        orderRepository.deleteById(Id);
        return Id.toString();
    }
}
