package com.viki.coffeeshop.persistence;

import com.viki.coffeeshop.model.Customer;
import com.viki.coffeeshop.model.CustomerOrder;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Viki Ristova on 10/06/18.
 */

public interface OrderRepository extends CrudRepository<CustomerOrder,Long> {

   /* Customer findAllByCustomerId(Long id);*/
}
