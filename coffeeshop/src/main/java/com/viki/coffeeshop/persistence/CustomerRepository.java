package com.viki.coffeeshop.persistence;


import com.viki.coffeeshop.model.Customer;
import org.springframework.data.repository.CrudRepository;

/**
 * Created by Viki Ristova on 10/06/18.
 */

public interface CustomerRepository extends CrudRepository<Customer, Long> {

}
