package com.viki.coffeeshop.service;

import com.viki.coffeeshop.model.CustomerOrder;
import com.viki.coffeeshop.model.Product;
import com.viki.coffeeshop.persistence.CustomerRepository;
import com.viki.coffeeshop.persistence.OrderRepository;
import com.viki.coffeeshop.persistence.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.dao.DataRetrievalFailureException;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;

@Service
public class ProductService implements CommandLineRunner {
    @Autowired
    ProductRepository productRepository;

    @Autowired
    CustomerRepository customerRepository;

    @Autowired
    OrderRepository orderRepository;

    public List<CustomerOrder> getAllOrders(){

        return (List<CustomerOrder>)orderRepository.findAll();
    }
    public List<Product> getAllProducts(){
        return (List<Product>)productRepository.findAll();
    }
    public Optional<Product> getProductById(Long id){
        return productRepository.findById(id);
    }
    public Optional<CustomerOrder> getOrderById(Long id){
        return orderRepository.findById(id);}



    public Optional<CustomerOrder> deleteOrder(Long id){
        Optional<CustomerOrder> order=orderRepository.findById(id);
        if(order!=null){
            this.customerRepository.deleteById(id);
            return order;
        }
        else{throw new DataRetrievalFailureException("Order not found!");}

    }
    @Override
    public void run(String... strings) throws Exception {

        Product mocha = new Product();
        mocha.setProductName("Mocha");
        mocha.setProductPrice(3.95);

        Product capuccinno = new Product();
        capuccinno.setProductName("Capuccinno");
        capuccinno.setProductPrice(4.95);

        productRepository.save(mocha);
        productRepository.save(capuccinno);


    }


}
